
# Stage 1: Build the application (uses Java 17)
FROM maven:3.8.5-openjdk-17 AS builder

# Set working directory
WORKDIR /app

# Copy the project directory
COPY . .

# Install dependencies and build the application
RUN mvn clean package

# Stage 2: Package the application (uses slim Java 17 image)
FROM openjdk:17-slim AS runner

# Copy the built JAR from the builder stage
COPY --from=builder /app/target/*.jar app.jar

# Expose port 8080 (adjust if needed)
EXPOSE 8080

# Start the application
CMD ["java", "-jar", "app.jar"]

